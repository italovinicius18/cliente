#ifndef FUNCIONARIO_HPP
#define FUNCIONARIO_HPP

#include <string>

#include "pessoa.hpp"

class Funcionario: public Pessoa {
    private:
        float salario;
        string funcao;
    public:
        Funcionario();
        ~Funcionario();
        float get_salario();
        void set_salario(float salario);
        string get_funcao();
        void set_funcao(string funcao);
        void imprime_dados();  

};

#endif
