
#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

#include "pessoa.hpp"

using namespace std;

class Cliente : public Pessoa{
private:
    string data_de_cadastro;
    float total_de_compras;
public:
    // Métodos
    Cliente();  // Construtor
    ~Cliente(); // Destruror
    // Métodos acessores
    void set_data_de_cadastro(string data_de_cadastro);
    string get_data_de_cadastro();
    void set_total_de_compras(float total_de_compras);
    float get_total_de_compras();
    // Outros Métodos
    void imprime_dados();

};

#endif













