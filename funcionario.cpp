#include "funcionario.hpp"
#include <iostream>

Funcionario::Funcionario(){
    cout << "Construtor da classe Funcionário" << endl;
    set_salario(0);
    set_funcao("");
}
Funcionario::~Funcionario(){
    cout << "Destrutor da classe Funcionário" << endl;
}
float Funcionario::get_salario(){
    return salario;
}
void Funcionario::set_salario(float salario){
    this->salario = salario;
}
string Funcionario::get_funcao(){
    return funcao;
}
void Funcionario::set_funcao(string funcao){
    this->funcao = funcao;
}
void Funcionario::imprime_dados(){
    cout << "Nome: " << get_nome() << endl;
    cout << "CPF: " << get_cpf() << endl;
    cout << "Telefone: " << get_telefone() << endl;    
    cout << "Email: " << get_email() << endl;
    cout << "Função: " << get_funcao() << endl;
    cout << "Salario: " << get_salario() << endl;
} 








