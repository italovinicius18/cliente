#include <iostream>
#include "funcionario.hpp"
#include "cliente.hpp"
#include <string>

using namespace std;

string getString(){
    string valor;
    getline(cin, valor);
    return valor;
}

template <typename T1>

T1 getInput(){
    while(true){
    T1 valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}
/*
int getInt(){
    while(true){
    int valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}
long int getLongInt(){
    while(true){
    long int valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}
float getFloat(){
    while(true){
    float valor;
    cin >> valor;
    if(cin.fail()){
        cin.clear();
        cin.ignore(32767,'\n');
        cout << "Entrada inválida! Insira novamente: " << endl;
    }
    else{
        cin.ignore(32767,'\n');
        return valor;
    }
    }
}
*/
int main(){
    
    Cliente clientes[10];
    Funcionario funcionarios[10];

    int comando = -1;
    int contador_cliente = 0;
    int contador_funcionario = 0;
    cout << "Cadastro da Padaria" <<  endl;

    while(comando != 0){
        cout << "Criar novo cadastro: " << endl;
        cout << "(1) Inserir Cliente" << endl;
        cout << "(2) Inserir Funcionário" << endl;
        cout << "(3) Imprime clientes" << endl;
        cout << "(4) Imprime funcionários" << endl;
        cout << "(0) Sair" << endl;
        cout << ":" << endl;
        //cin >> comando;
        comando = getInput<int>(); //getInt();

        string nome;
        long int cpf;
        string telefone;
        string email;
        string data_de_cadastro;
        float total_de_compras;
        float salario;
        string funcao;

        switch(comando){
            case 1:
                cout << "Cadastre o cliente:" << endl;
                cout << "Nome: ";
                //cin >> nome;
                nome = getString(); 
                cout << "CPF: ";
                //cin >> cpf;
                //cpf = getLongInt();
                cpf = getInput<long int>();
                cout << "Telefone: ";
                //cin >> telefone;
                telefone = getString();
                cout << "Email: ";
                //cin >> email;
                email = getString();
                cout << "Data de cadastro: ";
                //cin >> data_de_cadastro;
                data_de_cadastro = getString();
                cout << "Total de Compras: ";
                //cin >> total_de_compras;
                //total_de_compras = getFloat();
                total_de_compras = getInput<float>();
                // Inserir dados no objeto Cliente
                clientes[contador_cliente].set_nome(nome);
                clientes[contador_cliente].set_cpf(cpf);
                clientes[contador_cliente].set_telefone(telefone);
                clientes[contador_cliente].set_email(email);
                clientes[contador_cliente].set_data_de_cadastro(data_de_cadastro);
                clientes[contador_cliente].set_total_de_compras(total_de_compras);
                contador_cliente++;
                break;
            case 2:
                cout << "Cadastre o funcionário:" << endl;
                cout << "Nome: ";
                nome  = getString();
                cout << "CPF: ";
                cpf = getInput<long int>();
                cout << "Telefone: ";
                telefone = getString();
                cout << "Email: ";
                email = getString();
                cout << "Salário: ";
                salario = getInput<float>();
                cout << "Função: ";
                funcao = getString();
                // Inserir dados no objeto Funcionário
                funcionarios[contador_funcionario].set_nome(nome);
                funcionarios[contador_funcionario].set_cpf(cpf);
                funcionarios[contador_funcionario].set_telefone(telefone);
                funcionarios[contador_funcionario].set_email(email);
                funcionarios[contador_funcionario].set_salario(salario);
                funcionarios[contador_funcionario].set_funcao(funcao);
                contador_funcionario++;
                break;
            case 3:
                for (int i = 0; i < contador_cliente; i++){
                    clientes[i].imprime_dados();
                    cout << "---------------------------------" << endl;
                }
                break;
            case 4:
                for (int i = 0; i < contador_funcionario; i++){
                    funcionarios[i].imprime_dados();
                    cout << "---------------------------------" << endl;
                }
                break;
            case 0:
                break;                
            default:
                cout << "Opção inválida!" << endl;



        } 

    }





    return 0;
}
